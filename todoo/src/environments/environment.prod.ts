// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDzEuEUqbhgbDkgnJjtF8SJUWZihUNO7Xo",
    authDomain: "todo-5e535.firebaseapp.com",
    databaseURL: "https://todo-5e535.firebaseio.com",
    projectId: "todo-5e535",
    storageBucket: "todo-5e535.appspot.com",
    messagingSenderId: "209465863139",
    appId: "1:209465863139:web:78ff9927edfc219254ced6",
    measurementId: "G-6E8K2ELV01"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
